﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace AspNetWebApiRest.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        // [Authorize]
        public IHttpActionResult Get()
        {
            String[] a =  new string[] { "value1", "value2" };

            if (checkToken())
                return BadRequest1();
            return Json(a);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        private bool checkToken()
        {
            HttpRequestHeaders headers = this.Request.Headers;
            string requestToken = string.Empty;
            string pwd = string.Empty;
            if (headers.Contains("Authorization"))
            {
                requestToken = headers.GetValues("Authorization").First();
            }
            
            string tokenConfig = System.Configuration.ConfigurationManager.AppSettings["token"];
            bool checkToken = !(string.Equals(requestToken, tokenConfig));
            return checkToken;
        }

        private IHttpActionResult BadRequest1()
        {
            return BadRequest("Invalid data.");
        }
        
    }
}
