﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetWebApiRest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.app_env = ConfigurationManager.AppSettings["app_env"];
            ViewBag.host_name = ConfigurationManager.AppSettings["host_name"];
            ViewBag.app_version = ConfigurationManager.AppSettings["app_version"];

            return View();
        }
    }
}
