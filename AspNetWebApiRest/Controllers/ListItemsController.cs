﻿using System;
using System.Web.Http;
using System.Collections.Generic;

using AspNetWebApiRest.Models;

namespace AspNetWebApiRest.Controllers
{
    public class ListItemsController : ApiController
    {
        private static List<CustomListItem> _listItems { get; set; } = new List<CustomListItem>() {
                new CustomListItem { Id = 1, Text = "StateName1" },
                new CustomListItem { Id = 2, Text = "StateName2" },
                new CustomListItem { Id = 3, Text = "StateName3" }
        };

        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
        public CustomListItem Get(int id)
        {
            var result = _listItems.Exists(item => item.Id == id );

            foreach (CustomListItem aItem in _listItems)
            {
                Console.WriteLine(aItem);
                if (aItem.Id == id)
                {
                    return aItem;
                }
            }

            return new CustomListItem();
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public IEnumerable<CustomListItem> Get()
        {
            return _listItems;
        }
    }
}