﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Linq;

using AspNetWebApiRest.Models;
using System.Net.Http.Formatting;
using System.Dynamic;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace AspNetWebApiRest.Controllers
{
    public class CurrentVersionController : ApiController
    {
        public string json;

        public CurrentVersionController()
        {
            this.GetData();
        }

        private void GetData()
        {
            this.json = File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/VersionConfig.json"));
        }

        // // GET api/<controller>
        // [Route("api/currentVersion")]
        // public HttpResponseMessage Get()
        // {
        //     this.GetData();

        //     return new HttpResponseMessage()
        //     {
        //         Content = new StringContent(this.json, Encoding.UTF8, "application/json"),
        //         StatusCode = HttpStatusCode.OK
        //     };
        // }

        // // GET api/<controller>/5
        // public string Get(int id)
        // {
        //     return "value";
        // }

        // POST api/<controller>
        //[Authorize]
        [HttpPost]
        [Route("api/currentVersion")]
        public IHttpActionResult CurrentVersion([FromBody]ResponseVersionModel response)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            // Read Data From VersionConfig.json
            this.GetData();

            // Get parameter platform
            PlatformModel platformData = (response.platform);

            // Query Json Data
            JArray jsonVal  = JValue.Parse(this.json) as JArray;

            // Set List Object VersionConfig.json
            List<CurrentVersionModel> versionAll = JsonConvert.DeserializeObject<List<CurrentVersionModel>>(this.json);

            // Init Data
            dynamic jArr = jsonVal;
            int index = 0;

            ResponseVersionModel responseVersion = new ResponseVersionModel();

            foreach (dynamic item in jArr)
            {
                Console.WriteLine("platform : {0} ",item.ToString());

                // TODO: Check platform
                if (versionAll[index].platform == platformData)
                {
                    int vIndex = 0;
                    foreach (dynamic platformObj in item)
                    {
                        // TODO: Check Latest Version
                        if (vIndex==0 && versionAll[index].Versions[vIndex].isLatest == true)
                        {
                            Console.WriteLine("platformObj : {0}", platformObj);
                            responseVersion = new ResponseVersionModel() {
                                platform = versionAll[index].platform,
                                versionCode = versionAll[index].Versions[vIndex].versionCode,
                                versionName = versionAll[index].Versions[vIndex].versionName,
                                isForceUpdate = versionAll[index].Versions[vIndex].isForceUpdate,
                            };
                            break;
                        }
                        vIndex++;
                    }
                }
                index++;
            }
            
            var unserializedContent = JsonConvert.DeserializeObject(responseVersion.ToString());
            return Json(unserializedContent);
        }

        // POST api/<controller>
        //[Authorize]
        [HttpGet]
        [Route("api/version/{platform}/{versionName}")]
        public IHttpActionResult Version([FromUri] PlatformModel platform, [FromUri] string versionName)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
            
            // Read Data From VersionConfig.json
            this.GetData();

            // Get parameter platform
            PlatformModel platformData = (platform);

            // Query Json Data
            JArray jsonVal  = JValue.Parse(this.json) as JArray;

            // Set List Object VersionConfig.json
            List<CurrentVersionModel> versionAll = JsonConvert.DeserializeObject<List<CurrentVersionModel>>(this.json);

            // Init Data
            dynamic jArr = jsonVal;
            int index = 0;
            dynamic sendObject = new ExpandoObject();

            foreach (dynamic item in jArr)
            {
                // TODO: Check platform
                if (versionAll[index].platform == platformData)
                {
                    int vIndex = 0;
                    foreach (dynamic platformObj in item)
                    {
                        // TODO: Check Latest Version
                        if (
                            // versionAll[index].Versions[vIndex].isLatest == true &&
                            string.Equals(versionAll[index].Versions[vIndex].versionName , versionName )
                            )
                        {
                            sendObject.platform = versionAll[index].platform;
                            sendObject.versionName = versionAll[index].Versions[vIndex].versionName;
                            sendObject.filePath = versionAll[index].Versions[vIndex].filePath;
                            break;
                        }
                        vIndex++;
                    }
                }
                index++;
            }

            // TODO: Get Path Physical
            string pathFile = System.Web.Hosting.HostingEnvironment.MapPath(@sendObject.filePath);

            // TODO: Converting Pdf file into bytes array  
            var dataBytes = File.ReadAllBytes(pathFile);  
            // TODO: Adding bytes to memory stream   
            var dataStream = new MemoryStream(dataBytes);

            return new installFileResult(dataStream, Request, $"MobileMonitor-Boonrawd-{versionName}.apk");
        }
        
        [HttpGet]
        [Route("api/download/{platform?}/{versionName?}")]
        public HttpResponseMessage Download([FromUri]PlatformModel platform = PlatformModel.IOS, [FromUri] string versionName = "4.2")
        {
            if (!ModelState.IsValid)
                return new HttpResponseMessage()
                {
                    Content = new StringContent("Not Found!", Encoding.UTF8, "text/html"),
                    StatusCode = HttpStatusCode.NotFound
                };

            string content1 = @"<html>
                <head>
                    <meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">
                    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"" /> 
                    <title>Install Boonrawd</title>
                    <style type=""text/css"">
                    body {
                        background: url(bkg.png) repeat #c5ccd4;
                        font-family: Helvetica, arial, sans-serif;
                    }
                    .congrats {
                        font-size: 16pt;
                        padding: 6px;
                        text-align: center;
                    }
                    .step {
                        background: white;
                        border: 1px #ccc solid;
                        border-radius: 14px;
                        padding: 4px 10px;
                        margin: 10px 0;
                    }
                    .instructions {
                        font-size: 10pt;
                    }
                    .arrow {
                        font-size: 15pt;
                    }
                    table {
                        width: 100%;
                    }
                    </style>
                </head>
                <body>

                <div class=""congrats"">Congrats! You""ve been invited to the beta of Boonrawd.</div>

                <div class=""step"">
                    <table>
                    <tr>
                        <td class=""instructions"">Install the<br />Boonrawd IOS App</td>
                        <td width=""24"" class=""arrow"">&rarr;</td>
                        <td width=""57"" class=""imagelink"">
                            ";

            string content2 = @"
                        </td>
                    </tr>
                    <tr>
                        <td class=""instructions"">Install the<br />Boonrawd Android App</td>
                        <td width=""24"" class=""arrow"">&rarr;</td>
                        <td width=""57"" class=""imagelink"">";

            string content3 = @"</td>
                    </tr>
                    
                    </table>
                </div>

                </body>
                </html>
            ";

            string hostConfig = System.Configuration.ConfigurationManager.AppSettings["host_name"];
            string url = ($"https://{hostConfig}/api/manifest/{versionName}/plist");
            string href = $"itms-services://?action=download-manifest&amp;url={url}";
            string tagIos = @"<a href="""+ href + @""" id=""text"" >< img src=""Boonrawd-icon.png"" height=""57"" width=""57"" /></a> ";
            string tagAndroid = @"<a href=""" + ($"https://{hostConfig}/api/version/Android/{versionName}/") + @""" >< img src=""Boonrawd-icon.png"" height=""57"" width=""57"" /></a>";

            string content = String.Concat(content1, tagIos, content2, tagAndroid, content3);

            return new HttpResponseMessage()
            {
                Content = new StringContent(content, Encoding.UTF8, "text/html"),
                StatusCode = HttpStatusCode.OK
            };
        }

        [HttpGet]
        [Route("api/manifest/{versionName}/{plist}")]
        public IHttpActionResult Manifest([FromUri]string versionName = "4.2",[FromUri]string plist = "plist")
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            string content1 = @"
                <!DOCTYPE plist PUBLIC '-//Apple//DTD PLIST 1.0//EN' 'http://www.apple.com/DTDs/PropertyList-1.0.dtd'>
                <plist version='1.0'>
                <dict>
                    <key>items</key>
                    <array>
                        <dict>
                            <key>assets</key>
                            <array>
                                <dict>
                                    <key>kind</key>
                                    <string>software-package</string>
                                    <key>url</key>
                                    <string>";

            string content2 = @"</string>
                                </dict>
                            </array>
                            <key>metadata</key>
                            <dict>
                                <key>bundle-identifier</key>
                                <string>com.boonrawd.nostramobilemonitor</string>
                                <key>bundle-version</key>
                                <string>4.0</string>
                                <key>kind</key>
                                <string>software</string>
                                <key>subtitle</key>
                                <string>41A472</string>
                                <key>title</key>
                                <string>Boonrawd NOSTRA Mobile Monitor</string>
                            </dict>
                        </dict>
                    </array>
                </dict>
                </plist>
                ";

            string hostConfig = System.Configuration.ConfigurationManager.AppSettings["host_name"];

            string url = $"https://{hostConfig}/api/ipa/{versionName}/MobileMonitor-Boonrawd"; 

            string content = String.Concat("<?xml version='1.0' encoding='UTF-8'?>", content1, url, content2);

            // TODO: Adding bytes to memory stream   
            var dataStream = new MemoryStream(Encoding.ASCII.GetBytes(content));

            return new installFileResult(dataStream, Request, $"{plist}.plist");
        }

        [HttpGet]
        [Route("api/ipa/{versionName}/{ipa}")]
        public IHttpActionResult Ipa([FromUri]string versionName = "4.2",[FromUri]string ipa = "MobileMonitor-Boonrawd")
        {
            PlatformModel platform = PlatformModel.IOS;

            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");
            
            // Read Data From VersionConfig.json
            this.GetData();

            // Get parameter platform
            PlatformModel platformData = (platform);

            // Query Json Data
            JArray jsonVal  = JValue.Parse(this.json) as JArray;

            // Set List Object VersionConfig.json
            List<CurrentVersionModel> versionAll = JsonConvert.DeserializeObject<List<CurrentVersionModel>>(this.json);

            // Init Data
            dynamic jArr = jsonVal;
            int index = 0;
            dynamic sendObject = new ExpandoObject();

            foreach (dynamic item in jArr)
            {
                // TODO: Check platform
                if (versionAll[index].platform == platformData)
                {
                    int vIndex = 0;
                    foreach (dynamic platformObj in item)
                    {
                        // TODO: Check Latest Version
                        if (
                            // versionAll[index].Versions[vIndex].isLatest == true &&
                            string.Equals( versionAll[index].Versions[vIndex].versionName , versionName)
                            )
                        {
                            sendObject.platform = versionAll[index].platform;
                            sendObject.versionName = versionAll[index].Versions[vIndex].versionName;
                            sendObject.filePath = versionAll[index].Versions[vIndex].filePath;
                            break;
                        }
                        vIndex++;
                    }
                }
                index++;
            }

            // TODO: Get Path Physical
            string pathFile = System.Web.Hosting.HostingEnvironment.MapPath(@sendObject.filePath);

            // TODO: Converting Pdf file into bytes array  
            var dataBytes = File.ReadAllBytes(pathFile);  
            // TODO: Adding bytes to memory stream   
            var dataStream = new MemoryStream(dataBytes);

            return new installFileResult(dataStream, Request, $"MobileMonitor-Boonrawd-{versionName}.ipa");
        }

        private PlatformModel convertToPlatform(string platformStr)
        {
            return (PlatformModel) Enum.Parse(typeof(PlatformModel), platformStr);
        }
        
        private bool checkToken()
        {
            HttpRequestHeaders headers = this.Request.Headers;
            string requestToken = string.Empty;
            string pwd = string.Empty;
            if (headers.Contains("Authorization"))
            {
                requestToken = headers.GetValues("Authorization").First();
            }

            // IEnumerable<string> headerValues = Request.Headers.GetValues("Authorization");
            string tokenConfig = System.Configuration.ConfigurationManager.AppSettings["token"];
            bool checkToken = !( (string.Equals(requestToken, tokenConfig)) && (ModelState.IsValid) ) ;
            return checkToken;
        }

    }

    class installFileResult : IHttpActionResult
    {
        MemoryStream fileStuff;
        string PdfFileName;
        HttpRequestMessage httpRequestMessage;
        HttpResponseMessage httpResponseMessage;

        public installFileResult(MemoryStream data, HttpRequestMessage request, string filename)
        {
            fileStuff = data;
            httpRequestMessage = request;
            PdfFileName = filename;
        }
        public System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            httpResponseMessage = httpRequestMessage.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(fileStuff);
            //httpResponseMessage.Content = new ByteArrayContent(fileStuff.ToArray());  
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = PdfFileName;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            return System.Threading.Tasks.Task.FromResult(httpResponseMessage);
        }
    }
}
