﻿using System;
using System.Web;

namespace AspNetWebApiRest.Models
{
    public enum PlatformModel
    {
        Android = 1,
        IOS = 2
    }
}