﻿using System;
using System.Dynamic;
using System.Web;

namespace AspNetWebApiRest.Models
{
    public class ResponseVersionModel
    {
        public PlatformModel platform { get; set; }
        public Int32 versionCode { get; set; }
        public String versionName { get; set; }
        public Boolean isForceUpdate { get; set; }

        public override string ToString()
        {
            dynamic obj = new ExpandoObject();
            obj.platform = platform;
            obj.versionCode = versionCode;
            obj.versionName = versionName;
            obj.isForceUpdate = isForceUpdate;

            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
    }
}
