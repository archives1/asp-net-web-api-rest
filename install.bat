echo Welcome to the Google Cloud SDK!

if [ -z "$CLOUDSDK_PYTHON" ]; then
 if [ -z "$(which python)" ]; then
  echo
  echo "To use the Google Cloud SDK, you must have Python installed and on your PATH."
  echo "As an alternative, you may also set the CLOUDSDK_PYTHON environment variable"
  echo "to the location of your Python executable."
  exit 1
 fi
 CLOUDSDK_PYTHON="python"
fi